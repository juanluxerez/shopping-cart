<?php

namespace App\Tests\Controller;

use App\Controller\ShoppingCartController;
use App\Entity\ShoppingCart;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ShoppingCartControllerTest extends WebTestCase
{
    /**
     * PHPUnit's data providers allow to execute the same tests repeated times
     * using a different set of data each time.
     * See https://symfony.com/doc/current/testing.html#testing-against-different-sets-of-data.
     *
     * @dataProvider getPublicUrls
     */
    public function testPublicUrls(string $url): void
    {
        $client = static::createClient();
        $client->request('GET', $url);

        $this->assertResponseIsSuccessful(sprintf('The %s public URL loads correctly.', $url));
    }

    /**
     * A good practice for tests is to not use the service container, to make
     * them more robust.
     */
    public function testCreateShoppingCart(): void
    {
        $client = static::createClient();
        // the service container is always available via the test client
        $shoppingCart = $client->getContainer()->get('doctrine')->getRepository(ShoppingCart::class)->find(1);
        $client->request('GET', sprintf('/shopping/cart/%s', $shoppingCart->getId()));

        $this->assertResponseIsSuccessful();
    }

    public function getPublicUrls(): ?\Generator
    {
        yield ['/shopping/cart/'];
    }
}
