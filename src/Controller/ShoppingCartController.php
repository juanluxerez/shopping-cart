<?php

namespace App\Controller;

use App\Entity\ShoppingCart;
use App\Form\ShoppingCartType;
use App\Repository\ShoppingCartRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/shopping/cart")
 */
class ShoppingCartController extends AbstractController
{
    /**
     * @Route("/", name="shopping_cart_index", methods={"GET"})
     */
    public function index(ShoppingCartRepository $shoppingCartRepository): Response
    {
        return $this->render('shopping_cart/index.html.twig', [
            'shopping_carts' => $shoppingCartRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="shopping_cart_new", methods={"GET","POST"})
     */
    public function new(Request $request, LoggerInterface $logger): Response
    {
        $shoppingCart = new ShoppingCart();
        $form = $this->createForm(ShoppingCartType::class, $shoppingCart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            // Getting current discounts.
            $discounts = $entityManager->getRepository('App:Discount')
                ->findAll();
            $currentCartValue = 0;
            // New shopping cart is being created.
            if(empty($shoppingCart->getProducts()->getValues()))
                $shoppingCart->setValue(0);
            foreach ($shoppingCart->getProducts() as $product) {
                // Applying discounts if that's the case.
                foreach ($discounts as $discount) {
                    if($discount->getProduct()) {
                        if($discount->getProduct()->getId() == $product->getId()) {
                            // Product matches!
                            if($discount->getType() == 0) // It's percent, but this can change...
                                $shoppingCart->setValue($shoppingCart->getValue() + ($product->getPrice() / ($discount->getValue() * 100)));
                            else
                                $shoppingCart->setValue($shoppingCart->getValue() + ($product->getPrice() - $discount->getValue()));
                        }
                    }
                    if($discount->getCategory()) {
                        if($discount->getCategory()->getId() == $product->getCategory()->getId())
                            // Category matches!
                            if($discount->getType()->getId() == 0) // It's percent, but this can change...
                                $shoppingCart->setValue($shoppingCart->getValue() + ($product->getPrice() / ($discount->getValue() * 100)));
                            else
                                $shoppingCart->setValue($shoppingCart->getValue() + ($product->getPrice() - $discount->getValue()));
                    }
                }
            }
            // Searching for a customer group discount.
            foreach ($discounts as $discount) {
                if($discount->getCustomerGroup() == $shoppingCart->getCustomer()->getId()) {
                    // Customer group matches!
                    if($discount->getType()->getId() == 0) // It's percent, but this can change...
                        $cartDiscount = $shoppingCart->getValue() / ($discount->getValue() * 100);
                    else
                        $cartDiscount = $shoppingCart->getValue() - $discount->getValue();
                    // Let's discount the value.
                    $shoppingCart->setValue($shoppingCart->getValue() - $cartDiscount);
                }
            }
            // Setting unique reference.
            $shoppingCart->setReference(uniqid("SHOPCART_"));

            $entityManager->persist($shoppingCart);
            $entityManager->flush();
            $logger->notice('A new shopping cart was added: '. $shoppingCart->getId());

            return $this->redirectToRoute('shopping_cart_index');
        }

        return $this->render('shopping_cart/new.html.twig', [
            'shopping_cart' => $shoppingCart,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="shopping_cart_show", methods={"GET"})
     */
    public function show(ShoppingCart $shoppingCart): Response
    {
        return $this->render('shopping_cart/show.html.twig', [
            'shopping_cart' => $shoppingCart,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="shopping_cart_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ShoppingCart $shoppingCart): Response
    {
        $form = $this->createForm(ShoppingCartType::class, $shoppingCart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('shopping_cart_index');
        }

        return $this->render('shopping_cart/edit.html.twig', [
            'shopping_cart' => $shoppingCart,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="shopping_cart_delete", methods={"POST"})
     */
    public function delete(Request $request, ShoppingCart $shoppingCart): Response
    {
        if ($this->isCsrfTokenValid('delete'.$shoppingCart->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($shoppingCart);
            $entityManager->flush();
        }

        return $this->redirectToRoute('shopping_cart_index');
    }
}
