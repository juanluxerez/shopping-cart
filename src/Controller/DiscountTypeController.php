<?php

namespace App\Controller;

use App\Entity\DiscountType;
use App\Form\DiscountTypeType;
use App\Repository\DiscountTypeRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/discount/type")
 */
class DiscountTypeController extends AbstractController
{
    /**
     * @Route("/", name="discount_type_index", methods={"GET"})
     */
    public function index(DiscountTypeRepository $discountTypeRepository): Response
    {
        return $this->render('discount_type/index.html.twig', [
            'discount_types' => $discountTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="discount_type_new", methods={"GET","POST"})
     */
    public function new(Request $request, LoggerInterface $logger): Response
    {
        $discountType = new DiscountType();
        $form = $this->createForm(DiscountTypeType::class, $discountType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($discountType);
            $entityManager->flush();

            $logger->notice('A new discount type was added: '. $discountType->getId());

            return $this->redirectToRoute('discount_type_index');
        }

        return $this->render('discount_type/new.html.twig', [
            'discount_type' => $discountType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="discount_type_show", methods={"GET"})
     */
    public function show(DiscountType $discountType): Response
    {
        return $this->render('discount_type/show.html.twig', [
            'discount_type' => $discountType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="discount_type_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DiscountType $discountType): Response
    {
        $form = $this->createForm(DiscountTypeType::class, $discountType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('discount_type_index');
        }

        return $this->render('discount_type/edit.html.twig', [
            'discount_type' => $discountType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="discount_type_delete", methods={"POST"})
     */
    public function delete(Request $request, DiscountType $discountType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$discountType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($discountType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('discount_type_index');
    }
}
