<?php

namespace App\Controller;

use App\Entity\CustomerGroup;
use App\Form\CustomerGroupType;
use App\Repository\CustomerGroupRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CustomerGroupController
 * @package App\Controller
 * @Route("/customer/group")
 */
class CustomerGroupController extends AbstractController
{
    /**
     * @Route("/", name="customer_group_index", methods={"GET"})
     */
    public function index(CustomerGroupRepository $customerGroupRepository): Response
    {
        return $this->render('customer_group/index.html.twig', [
            'customer_groups' => $customerGroupRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="customer_group_new", methods={"GET","POST"})
     */
    public function new(Request $request, LoggerInterface $logger): Response
    {
        $customerGroup = new CustomerGroup();
        $form = $this->createForm(CustomerGroupType::class, $customerGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($customerGroup);
            $entityManager->flush();

            $logger->notice('A new customer group was added: '. $customerGroup->getId());

            return $this->redirectToRoute('customer_group_index');
        }

        return $this->render('customer_group/new.html.twig', [
            'customer_group' => $customerGroup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="customer_group_show", methods={"GET"})
     */
    public function show(CustomerGroup $customerGroup): Response
    {
        return $this->render('customer_group/show.html.twig', [
            'customer_group' => $customerGroup,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="customer_group_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CustomerGroup $customerGroup): Response
    {
        $form = $this->createForm(CustomerGroupType::class, $customerGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('customer_group_index');
        }

        return $this->render('customer_group/edit.html.twig', [
            'customer_group' => $customerGroup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="customer_group_delete", methods={"POST"})
     */
    public function delete(Request $request, CustomerGroup $customerGroup): Response
    {
        if ($this->isCsrfTokenValid('delete'.$customerGroup->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($customerGroup);
            $entityManager->flush();
        }

        return $this->redirectToRoute('customer_group_index');
    }
}
